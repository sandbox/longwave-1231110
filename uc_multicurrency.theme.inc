<?php

/**
 * Theme product currency switcher dropdown.
 */
function theme_uc_multicurrency_product_switcher() {
  $output = '<div class="uc-multicurrency-product-switcher">';
  $output .= drupal_get_form('uc_multicurrency_select_form');
  $output .= '</div>';
  return $output;
}

/**
 * Theme currency switcher list.
 */
function theme_uc_multicurrency_select_list() {
  $currencies = array_merge(
    array(variable_get('uc_currency_code', 'USD')),
    array_keys(variable_get('uc_multicurrency_settings', array()))
  );

  $options = array();
  foreach ($currencies as $code) {
    $options[] = array(
      'title' => theme('uc_multicurrency_option', $code),
      'href' => url($_GET['q'], array('query' => array('currency' => $code))),
    );
  }

  return theme('links', $options);
}

/**
 * Theme currency code for currency switchers.
 */
function theme_uc_multicurrency_option($code) {
  $options = uc_multicurrency_options($code);
  return t('!code - !sign', array('!code' => $code, '!sign' => $options['sign']));
}
