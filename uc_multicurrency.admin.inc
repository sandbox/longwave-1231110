<?php

/**
 * Multicurrency settings form.
 *
 * @ingroup forms
 */
function uc_multicurrency_settings_form() {
  $context = array('revision' => 'formatted');

  $form['currencies'] = array(
    '#tree' => TRUE,
    '#theme' => 'uc_multicurrency_settings',
    '#header' => array(
      'code'       => t('ISO code'),
      'example'    => t('Example'),
      'sign'       => t('Symbol'),
      'sign_after' => t('Sign after?'),
      'thou'       => t('Thousands marker'),
      'dec'        => t('Decimal marker'),
      'prec'       => t('Decimal places'),
      'factor'     => t('Conversion factor'),
    ),
  );
  $form['currencies']['default']['code'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('uc_currency_code', 'USD'),
    '#maxlength' => 3,
    '#size' => 3,
  );
  $form['currencies']['default']['example'] = array(
    '#type' => 'textfield',
    '#value' => uc_price(1000.1234, $context + array('currency' => variable_get('uc_currency_code', 'USD'))),
    '#disabled' => TRUE,
    '#size' => 10,
  );
  $form['currencies']['default']['sign'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('uc_currency_sign', '$'),
    '#size' => 5,
  );
  $form['currencies']['default']['sign_after'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('uc_sign_after_amount', FALSE),
  );
  $form['currencies']['default']['thou'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('uc_currency_thou', ','),
    '#size' => 1,
  );
  $form['currencies']['default']['dec'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('uc_currency_dec', '.'),
    '#size' => 1,
  );
  $form['currencies']['default']['prec'] = array(
    '#type' => 'select',
    '#options' => drupal_map_assoc(array(0, 1, 2)),
    '#default_value' => variable_get('uc_currency_prec', 2),
  );
  $form['currencies']['default']['factor'] = array(
    '#value' => '1.0',
  );

  $currencies = variable_get('uc_multicurrency_settings', array());
  foreach ($currencies as $code => $currency) {
    $form['currencies'][$code]['code'] = array(
      '#type' => 'textfield',
      '#default_value' => $code,
      '#maxlength' => 3,
      '#size' => 3,
    );
    $form['currencies'][$code]['example'] = array(
      '#type' => 'textfield',
      '#value' => uc_price(1000.1234 * $currency['factor'], $context + array('currency' => $code)),
      '#disabled' => TRUE,
      '#size' => 10,
    );
    $form['currencies'][$code]['sign'] = array(
      '#type' => 'textfield',
      '#default_value' => $currency['sign'],
      '#size' => 5,
    );
    $form['currencies'][$code]['sign_after'] = array(
      '#type' => 'checkbox',
      '#default_value' => $currency['sign_after'],
    );
    $form['currencies'][$code]['thou'] = array(
      '#type' => 'textfield',
      '#default_value' => $currency['thou'],
      '#size' => 1,
    );
    $form['currencies'][$code]['dec'] = array(
      '#type' => 'textfield',
      '#default_value' => $currency['dec'],
      '#size' => 1,
    );
    $form['currencies'][$code]['prec'] = array(
      '#type' => 'select',
      '#options' => drupal_map_assoc(array(0, 1, 2)),
      '#default_value' => $currency['prec'],
    );
    $form['currencies'][$code]['factor'] = array(
      '#type' => 'textfield',
      '#default_value' => $currency['factor'],
      '#size' => 5,
    );
  }

  $form['currencies']['new']['code'] = array(
    '#type' => 'textfield',
    '#default_value' => '',
    '#maxlength' => 3,
    '#size' => 3,
  );
  $form['currencies']['new']['sign'] = array(
    '#type' => 'textfield',
    '#default_value' => '',
    '#size' => 5,
  );
  $form['currencies']['new']['sign_after'] = array(
    '#type' => 'checkbox',
    '#default_value' => FALSE,
  );
  $form['currencies']['new']['thou'] = array(
    '#type' => 'textfield',
    '#default_value' => '',
    '#size' => 1,
  );
  $form['currencies']['new']['dec'] = array(
    '#type' => 'textfield',
    '#default_value' => '',
    '#size' => 1,
  );
  $form['currencies']['new']['prec'] = array(
    '#type' => 'select',
    '#options' => drupal_map_assoc(array(0, 1, 2)),
    '#default_value' => 2,
  );
  $form['currencies']['new']['factor'] = array(
    '#type' => 'textfield',
    '#default_value' => '',
    '#size' => 5,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  return $form;
}

function uc_multicurrency_settings_form_submit($form, &$form_state) {
  $values = $form_state['values']['currencies'];

  variable_set('uc_currency_code',     $values['default']['code']);
  variable_set('uc_currency_sign',     $values['default']['sign']);
  variable_set('uc_sign_after_amount', $values['default']['sign_after']);
  variable_set('uc_currency_thou',     $values['default']['thou']);
  variable_set('uc_currency_dec',      $values['default']['dec']);
  variable_set('uc_currency_prec',     $values['default']['prec']);
  unset($values['default']);

  $currencies = array();
  foreach ($values as $key => $currency) {
    if ($code = $currency['code']) {
      unset($currency['code']);
      unset($currency['example']);
      $currencies[$code] = $currency;
    }
  }
  variable_set('uc_multicurrency_settings', $currencies);
}

function theme_uc_multicurrency_settings($form) {
  $rows = array();
  foreach (element_children($form) as $key) {
    $row = array();
    foreach (array_keys($form['#header']) as $col) {
      $row[] = isset($form[$key][$col]) ? drupal_render($form[$key][$col]) : '';
    }
    $rows[] = $row;
  }

  return theme('table', array_values($form['#header']), $rows) . drupal_render($form);
}
